import java.util.Scanner;


public class PrimePrinter {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number to determine if it is prime or not");
        int userInput = scanner.nextInt();

        PrimeGenerator generatorObject = new PrimeGenerator();
        generatorObject.nextPrime(userInput);
        generatorObject.isPrime(userInput);

    }

}